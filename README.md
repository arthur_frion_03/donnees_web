# Projet Données Web
*Enora Langard & Arthur Frion*

## Démarrer les différents exercices :
Tous les résultats se trouvent dans le dossier output avec pour nom Ex[X]Res.xml

### Ex1 :
    Lancer le script ./launcher/launch_ex1.cmd
    ou
    Lancer le script ./launcher/launch_ex1.sh
### Ex2Sax :
    Lancer le script ./launcher/launch_ex2Sax.cmd
    ou
    Lancer le script ./launcher/launch_ex2Sax.sh
### Ex2Dom :
    Lancer le script ./launcher/launch_ex2Dom.cmd
    ou
    Lancer le script ./launcher/launch_ex2Dom.sh
### Ex2DomXpath :
    Lancer le script ./launcher/launch_ex2DomXpath.cmd
    ou
    Lancer le script ./launcher/launch_ex2DomXpath.sh
    

    
Attention, les fichiers comportent des soucis d'accentuation sous certains éditeurs pour des raisons inconnues.

