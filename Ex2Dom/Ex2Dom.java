package Ex2Dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

class President {
    private String nom;
    private final Map<String, Pays> pays;

    @Override
    public String toString() {
        final String[] ret = {"\t<président nom=\"" + getNom() + "\">\n"};
        getPays().forEach((s, pays) -> ret[0] += pays.toString());
        ret[0] += "\t</président>";
        return ret[0];
    }

    public President(String nom) {
        this.nom = nom;
        this.pays = new TreeMap<>();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Map<String, Pays> getPays() {
        return pays;
    }

    public void setPays(Map<String, Pays> pays) {
        //Pour des instances de Pays indépendantes
        pays.forEach((k, v) -> this.pays.put(k, new Pays(v.getNom(), v.getFrancophone())));
    }

    public void setDuree(String pays, int duree) {
        getPays().get(pays).setDuree(getPays().get(pays).getDuree() + duree + 1);
    }
}

class Pays {
    private String nom;
    private final String francophone;
    private Integer duree;

    public Pays(String nom, String francophone) {
        this.nom = nom;
        this.duree = 0;
        this.francophone = francophone;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getFrancophone() {
        return francophone;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    @Override
    public String toString() {
        final String[] ret = {"\t\t<pays nom=\"" + getNom() + "\" " +
                "durée=\"" + getDuree() + "\""};
        if (!francophone.equals("")) {
            ret[0] += " franchophone=\"" + getFrancophone() + "\"";
        }
        ret[0] += "/>\n";
        return ret[0];
    }
}

public class Ex2Dom {

    public Document doc;

    //Création du doc DOM
    public void load(String fichier) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(fichier);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Ex2Dom vs = new Ex2Dom();
        vs.load("lib/tp.xml");
        vs.analyse();
    }

    private void analyse() throws RuntimeException {//Récupération de l'élément racine
        final Element racine = doc.getDocumentElement();
        final Map<String, President> presidents = new TreeMap<>();
        final Map<String, Pays> paysPresident = new TreeMap<>();
        System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE liste-présidents\n" +
                "SYSTEM '../expected/res.dtd'>\n" +
                "<liste-présidents>");

        //Récupération des noeuds fils
        Element listePersonne = getElementFromEnd(racine);

        //Récupération de tous les présidents
        NodeList listeNodePersonnes = listePersonne.getChildNodes();
        getPresident(presidents, listeNodePersonnes);

        //Récupération tous les pays
        Node listePays = listePersonne.getPreviousSibling();
        while (listePays.getNodeType() != Node.ELEMENT_NODE || !listePays.getNodeName().equals("liste-pays")) {
            listePays = listePays.getPreviousSibling();
        }
        NodeList listeNodePays = listePays.getChildNodes();
        //Pour chaque noeud d'un pays
        getPaysAfrican(paysPresident, listeNodePays);

        //Affectation de la liste des pays à chaque président
        presidents.forEach((k, v) -> v.setPays(paysPresident));


        // Gestion de la partie durée
        calcDuree(racine, presidents, paysPresident);

        //Affichage final
        presidents.forEach((k, president) -> System.out.println(president.toString()));
        System.out.println("</liste-présidents>");
    }

    //Récupération de la liste des présidents
    private static void getPresident(Map<String, President> presidents, NodeList listeNodePersonnes) {
        for (int i = 0; i < listeNodePersonnes.getLength(); i++) {
            Node personne = listeNodePersonnes.item(i);
            if (personne.getNodeType() == Node.ELEMENT_NODE) {
                Element elementPersonne = (Element) personne;
                Node nodeFonction = personne.getFirstChild();
                while (nodeFonction.getNodeType() != Node.ELEMENT_NODE) {
                    nodeFonction = nodeFonction.getNextSibling();
                }
                Element fonction = (Element) nodeFonction;
                if (fonction.getAttribute("type").equals("Président de la République")) {
                    presidents.put(fonction.getAttribute("xml:id"), new President(elementPersonne.getAttribute("nom")));
                }
            }
        }
    }

    private static void getPaysAfrican(Map<String, Pays> paysPresident, NodeList listeNodePays) {
        for (int i = 0; i < listeNodePays.getLength(); i++) {
            Node pays = listeNodePays.item(i); //Récupère le pays à l'indexe i de la liste de pays
            //On vérifie qu'il ne s'agit pas d'un commentaire
            if (pays.getNodeType() == Node.ELEMENT_NODE) {
                Element elementPays = (Element) pays;
                boolean flag = false;
                NodeList nodeChildPays = pays.getChildNodes();
                for (int j = 0; j < nodeChildPays.getLength(); j++) {
                    Node nodeChild = nodeChildPays.item(j);
                    //Si le noeud est encompassed alors on regarde si le pays est en afrique
                    if (nodeChild.getNodeName().equals("encompassed")) {
                        Element encompossed = (Element) nodeChild;
                        if (encompossed.getAttribute("continent").equals("africa")) {
                            flag = true;
                        }
                    }
                    //Gestion de la partie francophone
                    if (nodeChild.getNodeName().equals("language") && flag) {
                        //Soucis ici si language.text() est vide
                        String textLanguage = nodeChild.getFirstChild().getNodeValue();
                        if (textLanguage.equals("French")) {
                            Element language = (Element) nodeChild;
                            if (language.getAttribute("percentage").isEmpty()) {
                                paysPresident.put(elementPays.getAttribute("xml:id"), new Pays(elementPays.getAttribute("nom"), "Officiel"));
                            } else {
                                if (Double.parseDouble(language.getAttribute("percentage")) > 30) {
                                    paysPresident.put(elementPays.getAttribute("xml:id"), new Pays(elementPays.getAttribute("nom"), "En-partie"));
                                } else {
                                    paysPresident.put(elementPays.getAttribute("xml:id"), new Pays(elementPays.getAttribute("nom"), ""));
                                }
                            }
                        } else {
                            paysPresident.put(elementPays.getAttribute("xml:id"), new Pays(elementPays.getAttribute("nom"), ""));
                        }
                    }
                }
            }
        }
    }

    //Calcul de l'attribut durée de la visite
    private static void calcDuree(Element racine, Map<String, President> presidents, Map<String, Pays> paysPresident) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Node nodeListeVisite = racine.getFirstChild();
        while (nodeListeVisite.getNodeType() != Node.ELEMENT_NODE || !nodeListeVisite.getNodeName().equals("liste-visites")) {
            nodeListeVisite = nodeListeVisite.getNextSibling();
        }
        Element listeVisite = (Element) nodeListeVisite;
        NodeList listeNodeVisite = listeVisite.getChildNodes();
        for (int i = 0; i < listeNodeVisite.getLength(); i++) {
            Node nodeVisite = listeNodeVisite.item(i);
            if (nodeVisite.getNodeType() == Node.ELEMENT_NODE) {
                Element elementVisite = (Element) nodeVisite;
                // Si on est sur le président courant alors on calcule la durée de visite pour le pays courant
                if (presidents.containsKey(elementVisite.getAttribute("personne")) && paysPresident.containsKey(elementVisite.getAttribute("pays"))) {
                    try {
                        presidents.get(elementVisite.getAttribute("personne")).setDuree(elementVisite.getAttribute("pays"),
                                (int) TimeUnit.DAYS.convert(Math.abs(sdf.parse(elementVisite.getAttribute("debut")).getTime() - sdf.parse(elementVisite.getAttribute("fin")).getTime()),
                                        TimeUnit.MILLISECONDS));
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    private static Element getElementFromEnd(Element element) {
        Node tempNode = element.getLastChild();
        while (tempNode.getNodeType() != Node.ELEMENT_NODE) {
            tempNode = tempNode.getPreviousSibling();
        }
        return (Element) tempNode;
    }

}
