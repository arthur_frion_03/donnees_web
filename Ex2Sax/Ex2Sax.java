package Ex2Sax;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;


class Pays {
    String nom;
    String francophone;

    public Pays(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getFrancophone() {
        return francophone;
    }

    public void setFrancophone(String francophone) {
        this.francophone = francophone;
    }
}

class Visite {
    int duree;


    public Visite(int duree) {
        this.duree = duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public int getDuree() {
        return duree;
    }
}

class DocReader extends DefaultHandler {
    Map<String, Pays> listePays = new TreeMap<>();
    Map<String, Map<String, Visite>> listeVisite = new TreeMap<>();
    Pays currentPays = new Pays("");
    String currentId;
    String percent;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String personne;

    @Override
    public void setDocumentLocator(Locator locator) {

    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("<?xml version='1.0' encoding='UTF-8' ?>");
        System.out.println("<!DOCTYPE liste-présidents SYSTEM '../expected/res.dtd'>");
        System.out.println("<liste-présidents>");
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("</liste-présidents>");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        //Pour Pays
        if (localName.equals("pays")) {
            this.currentPays.setNom(atts.getValue("nom"));
            this.currentId = atts.getValue("xml:id");
        }
        if (localName.equals("encompassed")) {
            if (atts.getValue("continent").equals("africa")) {
                this.listePays.put(this.currentId, new Pays(currentPays.getNom()));
            }
        }
        if (localName.equals("language") && this.listePays.containsKey(this.currentId)) {
            percent = null;
            if (atts.getValue("percentage") == null) {
                percent = "Officiel";
            } else {
                if (Float.parseFloat(atts.getValue("percentage")) > 30) {
                    percent = "En-partie";
                }
            }
        }
        //Pour durée
        if (localName.equals("visite")) {
            if (listePays.containsKey(atts.getValue("pays"))) {
                try {
                    if (listeVisite.containsKey(atts.getValue("pays"))) {
                        if (listeVisite.get(atts.getValue("pays")).containsKey(atts.getValue("personne"))) {
                            Visite temp = listeVisite.get(atts.getValue("pays")).get(atts.getValue("personne"));
                            temp.setDuree(temp.getDuree() + (int) TimeUnit.DAYS.convert(Math.abs(sdf.parse(atts.getValue("debut")).getTime() - sdf.parse(atts.getValue("fin")).getTime()), TimeUnit.MILLISECONDS) + 1);
                        } else {
                            listeVisite.get(atts.getValue("pays")).put(atts.getValue("personne"), new Visite((int) TimeUnit.DAYS.convert(Math.abs(sdf.parse(atts.getValue("debut")).getTime() - sdf.parse(atts.getValue("fin")).getTime()), TimeUnit.MILLISECONDS) + 1));
                        }
                    } else {
                        Map<String, Visite> temp = new TreeMap<>();
                        temp.put(atts.getValue("personne"), new Visite((int) TimeUnit.DAYS.convert(Math.abs(sdf.parse(atts.getValue("debut")).getTime() - sdf.parse(atts.getValue("fin")).getTime()), TimeUnit.MILLISECONDS) + 1));
                        listeVisite.put(atts.getValue("pays"), temp);
                    }
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        //Pour Président
        if (localName.equals("personne")) {
            personne = atts.getValue("nom");
        }
        if (localName.equals("fonction")) {
            if (atts.getValue("type").equals("Président de la République")) {
                System.out.println("<président nom=\"" + personne + "\">");
                listePays.forEach((name, vPays) -> {
                    System.out.print("<pays " +
                            "nom=\"" + vPays.getNom() + "\" ");
                    if (listeVisite.get(name).get(atts.getValue("xml:id")) != null) {
                        System.out.print("durée=\"" + listeVisite.get(name).get(atts.getValue("xml:id")).getDuree() + "\" ");
                    } else {
                        System.out.print("durée=\"0\" ");
                    }
                    if (vPays.getFrancophone() != null) {
                        System.out.print("franchophone=\"" + vPays.getFrancophone() + "\"");
                    }
                    System.out.println("/>");
                });
                System.out.println("</président>");
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String text = (new String(ch, start, length));
        if (text.equals("French") && listePays.containsKey(currentId)) {
            Pays temp = listePays.get(currentId);
            temp.setFrancophone(percent);
            listePays.replace(currentId, temp);
        }
    }
}

public class Ex2Sax {
    public static void main(String[] args) {
        DefaultHandler handler = new DocReader();
        try {
            XMLReader saxParser = XMLReaderFactory.createXMLReader();
            saxParser.setContentHandler(handler);
            saxParser.setErrorHandler(handler);
            saxParser.parse("./lib/tp.xml");
        } catch (Throwable t) {
            t.printStackTrace();
        }
        System.exit(0);
    }
}
