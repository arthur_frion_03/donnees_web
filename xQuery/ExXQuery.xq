xquery version "1.0" encoding "utf-8";

declare namespace saxon="http://saxon.sf.net/";

declare option saxon:output
"doctype-system=./res.dtd";

(: Pour afficher la balise personne, appelle setDuree :)
declare function local:setPersonne($personne as element(personne)) as element(président){
    <président nom='{$personne/@nom}'>{
        for $pays in fn:doc("./tp.xml")/déplacements/liste-pays/pays
        where some $continent in $pays/encompassed/@continent satisfies $continent eq 'africa'
        return local:setDuree($pays, $personne)}
    </président>
};
(: Pour calculer la durée, appelle la méthode setFrancophone :)
declare function local:setDuree($pays as element(pays), $personne as element(personne)) as element(pays)*{
    local:setFrancophone($pays,fn:count(fn:doc("./tp.xml")/déplacements/liste-visites/visite[@pays eq $pays/@xml:id][@personne eq $personne/fonction[./@type eq 'Président de la République']/@xml:id])+fn:sum(fn:doc("./tp.xml")/déplacements/liste-visites/visite[@pays eq $pays/@xml:id][@personne eq $personne/fonction[./@type eq 'Président de la République']/@xml:id]/fn:days-from-duration(xs:date(@fin) - xs:date(@debut))))
};

(: Pour calculer la francophonie, retourne l'élément Pays :)
declare function local:setFrancophone($pays as element(pays), $duree) as element(pays)*{
    if ($pays/language[./text() eq 'French'])
    then if ($pays/language[./text() eq 'French'][./@percentage])
         then if (xs:float($pays/language[./text() eq 'French']/@percentage) gt 30)
              then <pays nom='{$pays/@nom}' franchophone='En-partie' durée='{$duree}'/>
              else <pays nom='{$pays/@nom}' durée='{$duree}'/>
         else <pays nom='{$pays/@nom}' franchophone='Officiel' durée='{$duree}'/>
    else <pays nom='{$pays/@nom}' durée='{$duree}'/>
};

<liste-présidents>
    {for $personne in fn:doc("tp.xml")/déplacements/liste-personnes/personne
    where some $type in $personne/fonction/@type satisfies $type eq 'Président de la République'
    return local:setPersonne($personne)}
</liste-présidents>

