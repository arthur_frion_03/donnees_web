package Ex2DomXpath;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

class Pays {
    private String nom;
    private String francophone;

    private final Map<String, Integer> duree;

    public Pays(String nom) {
        this.nom = nom;
        this.duree = new TreeMap<>();
    }

    public Pays(String nom, String francophone) {
        this.nom = nom;
        this.francophone = francophone;
        this.duree = new TreeMap<>();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getFrancophone() {
        return francophone;
    }

    public Map<String, Integer> getDuree() {
        return duree;
    }

    public void putDuree(String personne, int duree) {
        getDuree().put(personne, duree);
    }
}

public class Ex2DomXpath {

    public Document doc;

    //Création du doc DOM
    public void load(String fichier) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(fichier);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Ex2DomXpath vs = new Ex2DomXpath();
        vs.load("lib/tp.xml");
        try {
            vs.analyse();
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    private void analyse() throws XPathExpressionException {
        Map<String, Pays> listePays = new TreeMap<>();
        Map<String, String> listePresidents = new TreeMap<>();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        XPathFactory xpathfactory = XPathFactory.newInstance();
        XPath xpath = xpathfactory.newXPath();
        System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE liste-présidents\n" +
                "SYSTEM '../expected/res.dtd'>\n" +
                "<liste-présidents>");

        getPaysAfricain(listePays, xpath);
        getPresidents(listePresidents, xpath);
        getDuree(listePays, listePresidents, sdf, xpath);
        affichage(listePays, listePresidents);
    }

    //Affichage de la liste des pays pour chaque président de la république (avec les informations concernant un pays)
    private static void affichage(Map<String, Pays> listePays, Map<String, String> listePresidents) {
        listePresidents.forEach((k, v) -> {
            System.out.println("\t<président nom=\"" + v + "\">");
            listePays.forEach((kpa, vpa) -> {
                System.out.print("\t\t<pays nom=\"" + vpa.getNom() + "\" durée=\"" + vpa.getDuree().get(k) + "\"");
                if (vpa.getFrancophone() != null) {
                    System.out.print(" franchophone=\"" + vpa.getFrancophone() + "\"");
                }
                System.out.println("/>");
            });
            System.out.println("\t</président>");
        });
        System.out.println("</liste-présidents>");
    }

    //Calcule de la durée de visite d'un président de la république dans un pays
    private void getDuree(Map<String, Pays> listePays, Map<String, String> listePresidents, SimpleDateFormat sdf, XPath xpath) {
        listePays.forEach((kpa, vpa) -> listePresidents.forEach((kpr, vpr) -> {
            String str = "/déplacements/liste-visites/visite[./@pays='" + kpa + "'][./@personne='" + kpr + "']";
            try {
                NodeList visite = (NodeList) xpath.compile(str).evaluate(doc, XPathConstants.NODESET);
                int tpsDuree = visite.getLength();
                for (int i = 0; i < visite.getLength(); i++) {
                    tpsDuree += (int) TimeUnit.DAYS.convert(
                            Math.abs(sdf.parse(visite.item(i).getAttributes().getNamedItem("debut").getNodeValue()).getTime() -
                                    sdf.parse(visite.item(i).getAttributes().getNamedItem("fin").getNodeValue()).getTime()),
                            TimeUnit.MILLISECONDS);
                }
                vpa.putDuree(kpr, tpsDuree);
            } catch (XPathExpressionException | ParseException e) {
                throw new RuntimeException(e);
            }
        }));
    }

    //Récupération de l'ensemble des présidents de la république
    private void getPresidents(Map<String, String> listePresidents, XPath xpath) throws XPathExpressionException {
        NodeList presidents = (NodeList) xpath.compile("/déplacements/liste-personnes/personne[./fonction/@type='Président de la République']").evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < presidents.getLength(); i++) {
            NodeList fonction = (NodeList) xpath.compile("/déplacements/liste-personnes/personne[./@nom=\"" + presidents.item(i).getAttributes().getNamedItem("nom").getNodeValue() + "\"]/fonction[@type='Président de la République']").evaluate(doc, XPathConstants.NODESET);
            listePresidents.put(fonction.item(0).getAttributes().getNamedItem("xml:id").getNodeValue(), presidents.item(i).getAttributes().getNamedItem("nom").getNodeValue());
        }
    }

    //Récupération de l'ensemble des pays africains et de l'information concernant la francophonie
    private void getPaysAfricain(Map<String, Pays> listePays, XPath xpath) throws XPathExpressionException {
        //Récupération des pays africains
        NodeList paysAfricain = (NodeList) xpath.compile("/déplacements/liste-pays/pays[./encompassed/@continent='africa']").evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < paysAfricain.getLength(); i++) {
            //Gestion de l'attribut francophonie du pays
            String str = "/déplacements/liste-pays/pays[./@name=\"" + paysAfricain.item(i).getAttributes().getNamedItem("name").getNodeValue() + "\"]/language[./text()='French']";
            NodeList langue = (NodeList) xpath.compile(str).evaluate(doc, XPathConstants.NODESET);
            if (langue.getLength() < 1) {
                listePays.put(paysAfricain.item(i).getAttributes().getNamedItem("xml:id").getNodeValue(), new Pays(paysAfricain.item(i).getAttributes().getNamedItem("nom").getNodeValue()));
            } else if (langue.item(0).getAttributes().getNamedItem("percentage") == null) {
                listePays.put(paysAfricain.item(i).getAttributes().getNamedItem("xml:id").getNodeValue(), new Pays(paysAfricain.item(i).getAttributes().getNamedItem("nom").getNodeValue(), "Officiel"));
            } else if (Float.parseFloat(langue.item(0).getAttributes().getNamedItem("percentage").getNodeValue()) > 30) {
                listePays.put(paysAfricain.item(i).getAttributes().getNamedItem("xml:id").getNodeValue(), new Pays(paysAfricain.item(i).getAttributes().getNamedItem("nom").getNodeValue(), "En-partie"));
            } else {
                listePays.put(paysAfricain.item(i).getAttributes().getNamedItem("xml:id").getNodeValue(), new Pays(paysAfricain.item(i).getAttributes().getNamedItem("nom").getNodeValue()));
            }
        }
    }
}
