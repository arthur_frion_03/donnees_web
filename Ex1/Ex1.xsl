<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               xmlns:fn="http://www.w3.org/2005/xpath-functions"
               xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="yes" doctype-system="../expected/res.dtd"/>
    <xsl:strip-space elements="*"/>

    <!--Récupération de l'ensemble des présidents-->
    <xsl:template match="/">
        <xsl:element name="liste-présidents">
            <xsl:apply-templates
                    select="/déplacements/liste-personnes/personne[./fonction/@type = 'Président de la République']"/>
        </xsl:element>
    </xsl:template>

    <!--Récupération de l'ensemble des pays Africains-->
    <xsl:template match="personne">
        <xsl:element name="président">
            <xsl:attribute name="nom">
                <xsl:value-of select="@nom"/>
            </xsl:attribute>
            <xsl:apply-templates select="/déplacements/liste-pays/pays[./encompassed/@continent = 'africa']">
                <xsl:with-param name="visiteur"
                                select="current()/fonction[@type = 'Président de la République']/@xml:id"/>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="pays">
        <xsl:param name="visiteur"/>
        <xsl:element name="pays">
            <xsl:attribute name="nom">
                <xsl:value-of select="./@nom"/>
            </xsl:attribute>
            <!--Calcul de l'attribut durée-->
            <xsl:attribute name="durée">
                <xsl:value-of
                        select="count(/déplacements/liste-visites/visite[@pays=current()/@xml:id]
                        [@personne=$visiteur])+
                        sum(/déplacements/liste-visites/visite[@pays=current()/@xml:id]
                        [@personne=$visiteur]/fn:days-from-duration(xs:date(@fin) - xs:date(@debut)))"/>
            </xsl:attribute>
            <!--Récupération de l'information sur la francophonie-->
            <xsl:choose>
                <xsl:when test="./language[./text()='French'][not(./@percentage)]">
                    <xsl:attribute name="franchophone">Officiel</xsl:attribute>
                </xsl:when>
                <xsl:when test="./language[./text()='French'][./@percentage > 30]">
                    <xsl:attribute name="franchophone">En-partie</xsl:attribute>
                </xsl:when>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
</xsl:transform>